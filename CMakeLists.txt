cmake_minimum_required(VERSION 3.16s)
project(SIMULATION-TISSUS)

set(CMAKE_BUILD_TYPE DEBUG)
find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIR})
include_directories(lib)
include_directories(lib/PxShared/include)
find_package(glfw3 3.2 REQUIRED)
find_package(OpenGL REQUIRED)
add_subdirectory(lib)


set(SRCS
    ./src/Camera.cpp
    ./src/main.cpp
    ./src/Mesh.cpp
    ./src/Object.cpp
    ./src/Objects.cpp
    ./src/Shader.cpp
    ./src/Textures.cpp
    ./src/Window.cpp
    ./src/World.cpp
    ./src/Worley.cpp
)


add_executable(run ${SRCS})
target_link_libraries(run FastNoise)
target_link_libraries(run glad)
target_link_libraries(run imgui)
target_link_libraries(run lodepng)
target_link_libraries(run stb_image)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysX.so)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysXCharacterKinematic_static.a)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysXCommon.so)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysXCooking.so)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysXExtensions_static.a)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysXFoundation.so)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysXPvdSDK_static.a)
target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/PhysX/libPhysXVehicle_static.a)



target_include_directories(run PUBLIC ${OPENGL_INCLUDE_DIR})
target_link_libraries(run glfw ${OPENGL_gl_LIBRARY})
if(${CMAKE_BUILD_TYPE} EQUAL "DEBUG")
    target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/NvCloth/libNvClothDebug.so)
    remove_definitions("NDEBUG")
else() 
    target_link_libraries(run ${CMAKE_CURRENT_SOURCE_DIR}/lib/NvCloth/libNvCloth.so)
    add_compile_definitions(NDEBUG)
endif()

message(WARNING "${CMAKE_BUILD_TYPE} = " ${CMAKE_BUILD_TYPE})

file(COPY data DESTINATION ${CMAKE_BINARY_DIR})
file(COPY shaders DESTINATION ${CMAKE_BINARY_DIR})